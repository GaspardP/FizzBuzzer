package moi.fizzbuzzer

import android.app.Application
import org.koin.android.ext.android.startKoin
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, modulesForProd())
    }

}


private fun modulesForProd(): List<Module> {
    val myModules : Module = applicationContext {
        factory { MyFizzBuzzer() as FizzBuzzer }
    }

    return listOf(myModules)
}

package moi.fizzbuzzer

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject


class MainActivity : Activity() {

    val fizzBuzzer: FizzBuzzer by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button_reset.setOnClickListener { fizzBuzzer.restart() }
        button_next.setOnClickListener { outputTextView.setText(fizzBuzzer.next())}
    }
}

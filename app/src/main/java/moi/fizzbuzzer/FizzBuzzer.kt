package moi.fizzbuzzer

interface FizzBuzzer {
    fun restart()
    fun next(): String
}

